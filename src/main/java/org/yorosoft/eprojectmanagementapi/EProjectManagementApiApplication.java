package org.yorosoft.eprojectmanagementapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EProjectManagementApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(EProjectManagementApiApplication.class, args);
    }

}
